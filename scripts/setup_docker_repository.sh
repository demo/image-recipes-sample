#!/bin/sh

set -eu

usage() {
	echo "$0 <debian-release>
	Setup docker repository for <debian-release>. E.g. \"buster\"

	options:
	 -h           Display this help and exit
	"
	exit 1
}

setup_docker_repository() {
	mkdir -p ${ROOTDIR}/usr/share/keyrings/
	mkdir -p ${ROOTDIR}/etc/apt/sources.list.d/
	curl -fsSL "https://download.docker.com/linux/debian/gpg" | \
		gpg --dearmor -o "${ROOTDIR}/usr/share/keyrings/docker-archive-keyring.gpg"
	echo "deb [arch=arm64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian ${debian_release} stable" | \
		tee "${ROOTDIR}/etc/apt/sources.list.d/docker.list" > /dev/null
}

if [ $# -ne 1 ]; then
	echo Error: Need debian release name. >&2
	echo >&2
	usage
	exit 1
fi

debian_release="$1"

setup_docker_repository

#!/bin/sh

# Copyright © 2015 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Reconfigure an Apertis target system for debugging and basic development.

set -e

die () {
    echo "$0: $@" >&2
    exit 1
}

packages="openssh-server rsync"
build=
build_dep=""
gpl3=
enable_debug=
disable_debug=

while [ "$#" -gt 0 ]; do
    case "$1" in
        (--help)
            echo "usage: sudo apertis-dev [OPTIONS]"
            echo "Enable debugging and basic development."
            echo "OPTIONS are:"
            echo "    -b|--build: install basic build tools"
            echo "    -d|--build-dep PACKAGE: install build-deps of PACKAGE"
            echo "    -g|--debug: install basic debugging tools"
            echo "    -3|--gpl3: install GPL3 versions of tar, coreutils"
            echo "    --enable-debug: enable debug logging from applications"
            echo "    --disable-debug: disable debug logging from applications"
            echo "For developer use only: do not do this in production."
            exit 0
            ;;
        (-b|--build)
            build=1
            shift
            ;;
        (-d|--build-dep)
            build=1
            build_dep="$build_dep $2"
            shift 2
            ;;
        (-g|--debug)
            # devscripts is mainly here so we have debi
            packages="$packages apertis-tests devscripts gdb strace valgrind bash-completion less systemd-coredump"
            shift
            ;;
        (-3|--gpl3)
            gpl3=1
            shift
            ;;
        (--enable-debug)
            enable_debug=1
            shift
            ;;
        (--disable-debug)
            disable_debug=1
            shift
            ;;
        (*)
            die "unknown option \"$1\""
            ;;
    esac
done

[ "$(id -u)" = 0 ] || die "must be run as root"

if [ -n "$enable_debug" ] && [ -n "$disable_debug" ]; then
    echo "Both --enable-debug and --disable-debug specified. Doing neither."
    enable_debug=
    disable_debug=
fi

if [ -n "$build" ]; then
    packages="$packages build-essential ccache devscripts fakeroot"
    packages="$packages pristine-tar python3-debian"
fi

mount -o remount,rw /
# replace "target" component with "development" unless "development" is
# already there
sed -i.orig -e '/\bdevelopment\b/! s/ target\( \|$\)/ target development\1/' /etc/apt/sources.list
apt update
apt-get -y install --no-install-recommends eatmydata
eatmydata apt-get -y install --no-install-recommends $packages
if [ -n "$build_dep" ]; then
    eatmydata apt-get -y build-dep --no-install-recommends $build_dep
fi

if [ -n "$gpl3" ]; then
    # Workaround for https://bugs.apertis.org/show_bug.cgi?id=626
    # dpkg will fail if there isn't a tar and an rm on $PATH
    cp /bin/rm /bin/tar /usr/local/bin
    apt-get -y install coreutils tar
    rm /usr/local/bin/rm /usr/local/bin/tar
fi

if [ -n "$enable_debug" ]; then
    # Enable debug logging for Canterbury and all the applications it spawns.
    mkdir -p /etc/systemd/user/canterbury.service.d
    cat << EOF > /etc/systemd/user/canterbury.service.d/apertis-dev.conf
[Service]
Environment=G_MESSAGES_DEBUG=all
EOF
    echo "Wrote /etc/systemd/user/canterbury.service.d/apertis-dev.conf"
fi

if [ -n "$disable_debug" ]; then
    # Undo what --enable-debug does.
    rm /etc/systemd/user/canterbury.service.d/apertis-dev.conf
    echo "Deleted /etc/systemd/user/canterbury.service.d/apertis-dev.conf"
fi

if [ -n "$enable_debug" ] || [ -n "$disable_debug" ]; then
    # The user needs to run `systemctl --user daemon-reload` and restart Canterbury,
    # but restarting Canterbury by itself will probably produce weird state; so just
    # advise to restart the system.
    echo "*** Now reboot the system, or stop and restart the SDK simulator."
fi

# vim:set sw=4 sts=4 et:
